import React from "react";
import PostListItem from '../post-list-item';
import '../post-list/post-list.css'

const PostList = ({posts, onDelete, onToggleLike, onToggleImportant}) => {

    const elements = posts.map((item) => {
        const {id, ...itemsProps} = item;
       return (
           <li key={id} className="list-group-item">
               <PostListItem
                   {...itemsProps}
                   onDelete={() => onDelete(id)}
                   onToggleImportant={() => onToggleImportant(id)}
                   onToggleLike={() => onToggleLike(id)}
                   // label={item.label}
                   // important={item.important}
               />
           </li>
       )
    });

    return(
        <ul className="app-list list-group">
            {elements}
        </ul>
    )
}

export default PostList